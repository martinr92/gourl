package gourl

import (
	"testing"
)

func TestInvalidInputType(t *testing.T) {
	s := "a"
	_, err := Marshal(s)
	if err == nil {
		t.Error("error expected, because an invalid input type was passed")
	}
}

type StructInvalidType struct {
	Text StructNonTags
}

func TestInvalidTypeInStruct(t *testing.T) {
	s := StructInvalidType{}
	_, err := Marshal(s)
	if err == nil {
		t.Error("error expected, because an unsupported field exists in the struct")
	}
}
func TestInvalidTypeInStructValues(t *testing.T) {
	s := StructInvalidType{}
	_, err := Values(s)
	if err == nil {
		t.Error("error expected, because an unsupported field exists in the struct")
	}
}

type StructNonTags struct {
	Text      string
	Number    int
	nonPublic int
}

var (
	TestStruct       = StructNonTags{Text: "text", Number: 123, nonPublic: 123}
	TestStructString = "Number=123&Text=text"
)

func TestNonTagsMarshal(t *testing.T) {
	data, err := Marshal(TestStruct)
	if err != nil {
		t.Error(err)
	}

	val := string(data)
	if val != TestStructString {
		t.Errorf("invalid string returned for non tag struct %s", val)
	}
}
func TestNonTagsValues(t *testing.T) {
	data, err := Values(TestStruct)
	if err != nil {
		t.Error(err)
	}

	val := data.Encode()
	if val != TestStructString {
		t.Errorf("invalid string returned for non tag struct %s", val)
	}
}

type StructTagged struct {
	Text        string  `url:"text"`
	TextPointer *string `url:"text_pointer"`
	NilPointer  *string `url:",omitempty"`
	NilText     string  `url:"nilText,omitempty"`
	Number      int     `url:"num"`
	NilNumber   int     `url:",omitempty"`
	IsGreat     bool    `url:"great"`
	Float32     float32 `url:"float"`
	NilFloat32  float32 `url:",omitempty"`
	Float64     float64 `url:"float64"`
	NilFloat64  float64 `url:",omitempty"`
	Ingored     string  `url:"-"`
}

func TestTagged(t *testing.T) {
	str := "text"
	s := StructTagged{Text: str, TextPointer: &str, Number: 123, IsGreat: true, Float32: 32.32, Float64: 32.32, Ingored: "ok"}
	data, err := Marshal(s)
	if err != nil {
		t.Error(err)
	}

	val := string(data)
	if val != "float=32.32&float64=32.32&great=true&num=123&text=text&text_pointer=text" {
		t.Errorf("invalid string returned for tagged struct %s", val)
	}
}
