// Copyright 2019 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gourl

import (
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

const (
	tagName = "url"
)

// Marshal converts a struct into a URL parameter compatible byte string.
func Marshal(v interface{}) ([]byte, error) {
	e, err := newEncoder(v)
	if err != nil {
		return nil, err
	}
	data := e.Values.Encode()
	return []byte(data), nil
}

// Values converts a struct into a map.
func Values(v interface{}) (url.Values, error) {
	e, err := newEncoder(v)
	if err != nil {
		return nil, err
	}
	return e.Values, nil
}

type encoder struct {
	Values url.Values
}

func newEncoder(v interface{}) (encoder, error) {
	e := encoder{}
	e.Values = make(url.Values)
	err := e.marshal(v)
	return e, err
}

func (e *encoder) marshal(v interface{}) error {
	val := reflect.ValueOf(v)
	if val.Kind() != reflect.Struct {
		return fmt.Errorf("invalid input type %s; struct expected", val.Kind())
	}

	t := val.Type()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		// ignore unexported fields
		if field.PkgPath != "" {
			continue
		}

		// parse tag
		tagString, _ := field.Tag.Lookup(tagName)
		if tagString == "" {
			// if not tag was set, use the name by default
			tagString = field.Name
		}
		tag := tagByString(tagString)

		// check for ignore
		if tag.Ignored {
			continue
		}

		// parse value
		v := val.Field(i)
		val, isNil, err := e.parseValue(v, tag)
		if err != nil {
			return err
		}

		// check omitempty
		if tag.OmitEmpty && isNil {
			continue
		}

		// append to response
		e.Values.Add(tag.Name, val)
	}

	return nil
}

func (e *encoder) parseValue(val reflect.Value, tag tag) (string, bool, error) {
	i := val.Interface()
	switch val.Kind() {
	case reflect.Bool:
		return strconv.FormatBool(val.Bool()), !val.Bool(), nil
	case reflect.Float32:
		isNil := false
		if val.Float() == 0 {
			isNil = true
		}
		return strconv.FormatFloat(val.Float(), 'f', -1, 32), isNil, nil
	case reflect.Float64:
		isNil := false
		if val.Float() == 0 {
			isNil = true
		}
		return strconv.FormatFloat(val.Float(), 'f', -1, 64), isNil, nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		isNil := false
		if val.Int() == 0 {
			isNil = true
		}
		return strconv.FormatInt(val.Int(), 10), isNil, nil
	case reflect.Ptr:
		if val.IsNil() {
			return "", true, nil
		}
		return e.parseValue(val.Elem(), tag)
	case reflect.String:
		text := i.(string)
		isNil := false
		if text == "" {
			isNil = true
		}
		return text, isNil, nil
	}

	return "", false, fmt.Errorf("unsupported field-type %s in struct; omit the value using '-' as tag", val.Kind())
}

type tag struct {
	Name       string
	Attributes []string
	Ignored    bool
	OmitEmpty  bool
}

func tagByString(s string) tag {
	t := tag{}
	t.parseString(s)
	return t
}

func (t *tag) parseString(s string) {
	if s == "-" {
		t.Ignored = true
		return
	}

	dat := strings.Split(s, ",")
	t.Name = dat[0]
	t.Attributes = dat[1:]

	for _, attr := range t.Attributes {
		if attr == "omitempty" {
			t.OmitEmpty = true
		}
	}
}
