# gourl
[![GoDoc](https://godoc.org/gitlab.com/martinr92/gourl?status.svg)](https://godoc.org/gitlab.com/martinr92/gourl)
[![pipeline status](https://gitlab.com/martinr92/gourl/badges/master/pipeline.svg)](https://gitlab.com/martinr92/gourl/commits/master)
[![coverage report](https://gitlab.com/martinr92/gourl/badges/master/coverage.svg)](https://gitlab.com/martinr92/gourl/commits/master)
[![codecov](https://codecov.io/gl/martinr92/gourl/branch/master/graph/badge.svg)](https://codecov.io/gl/martinr92/gourl)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/martinr92/gourl)](https://goreportcard.com/report/gitlab.com/martinr92/gourl)

gourl is a serialization framework for golang. It converts the fields of a struct into a URL parameters compatible format.
# Example
```golang
type Test struct {
    NumberField int    `url:"numberField"`
    TextField   string `url:"textField"`
}

test := Test{NumberField: 1, TextField: "hey"}

// returns the struct as []byte
byteData, err := gourl.Marshal(test)
text := string(byteData) // numberField=1&textField=hey

// returns the struct as url.Values
values, err := gourl.Values(test)
values.Encode() // numberField=1&textField=hey
```

# Features
The following data types are supported:
- bool
- float32, float64
- int, int8, int16, int32, int64
- string

Following tags are supported:
- Ignore fields in struct for serialization
```golang
type Test struct {
    IgnoredField string `url:"-"`
}
```
- Ignore the field if
  - pointer type is nil
  - type string is empty
  - type int or float is 0
  - type boolean is false
```golang
type Test struct {
    EmptyString string `url:"emptyString,omitempty"`
}
```

# License
```
Copyright 2019 Martin Riedl

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
